<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $fillable = [
        'name',
        'price',
        'short_desc',
        'description',
        'image',
        'status',
        'category_id',
    ];

    public function categories(){
        return $this->belongTo(Category::class,'category_id');
    }
}
