<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|string|max:255',
            'phone'     => 'required|string|max:255',
            'email'     => 'required|string|max:255',
            'address'   => 'required|string|max:255', 
        ]);

        $customers = Customer::create([
            'name'     => $request['name'],
            'gender'   => $request['gender'],
            'phone'    => $request['phone'],
            'email'    => $request['email'],
            'address'  => $request['address']  
        ]);

        return response()->json(['message' => "Customer was submit."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $customers = Customer::find($id);
        // return response()->json($customers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|string|max:255',
            'phone'     => 'required|string|max:255',
            'email'     => 'required|string|max:255',
            'address'   => 'required|string|max:255',
        ]);

        $customers = Customer::findOrFail($id);  
        $customers ->update($request->all());

        return response()->json(['message'=>'customer was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customers = Customer::destroy($id);

        return response()->json(['message'=>"delete successfully ."]);
    }
    public function getRow(){
        $customers = Customer::orderBy('id','desc')->paginate(10);
        return $customers;
    }
}
