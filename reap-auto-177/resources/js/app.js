

// const { inProduction } = require('laravel-mix');

require('./bootstrap');

import { Form, HasError, AlertError } from 'vform'

window.Vue = require('vue').default;
window.Form = Form;
window.Fire = new Vue();

import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2);


// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('customer', require('./components/Customer.vue').default);
Vue.component('product', require('./components/Product.vue').default);
Vue.component('category',require('./components/Category.vue').default);
Vue.component('brand',require('./components/Brand.vue').default);
Vue.component('post',require('./components/Post.vue').default);
Vue.component('setting',require('./components/Setting.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
