<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>

  {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="dist/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="dist/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="dist/plugins/jqvmap/jqvmap.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="dist/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="dist/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="dist/plugins/summernote/summernote-bs4.min.css"> --}}
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper" id="app">
  {{-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{url('dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
  </div> --}}

 @include('backend.partials.navbar')
 @include('backend.partials.sidebar')
   @yield('content')
   @yield('content-header')
@include('backend.partials.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

{{-- <script src="{{url('dist/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{url('dist/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script> --}}
{{-- <script src="dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="dist/plugins/chart.js/Chart.min.js"></script>
<script src="dist/plugins/sparklines/sparkline.js"></script>
<script src="dist/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="dist/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<script src="dist/plugins/jquery-knob/jquery.knob.min.js"></script>
<script src="dist/plugins/moment/moment.min.js"></script>
<script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
<script src="dist/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="dist/plugins/summernote/summernote-bs4.min.js"></script>
<script src="dist/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="dist/js/adminlte.js"></script>
<script src="dist/js/demo.js"></script>
<script src="dist/js/pages/dashboard.js"></script> --}}
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>