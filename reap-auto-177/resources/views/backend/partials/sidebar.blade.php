 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="index3.html" class="brand-link">
       <img src="{{url('dist/img/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
       <span class="brand-text font-weight-light">Reap Auto 177</span>
     </a>
 
     <!-- Sidebar -->
     <div class="sidebar">
       <!-- Sidebar user panel (optional) -->
       <div class="user-panel mt-3 pb-3 mb-3 d-flex">
         <div class="image">
           <img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
         </div>
         <div class="info">
           <a href="#" class="d-block">{{auth()->user()->getFullname()}}</a>
         </div>
       </div>
 
       {{-- <!-- SidebarSearch Form -->
       <div class="form-inline">
         <div class="input-group" data-widget="sidebar-search">
           <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
           <div class="input-group-append">
             <button class="btn btn-sidebar">
               <i class="fas fa-search fa-fw"></i>
             </button>
           </div>
         </div>
       </div> --}}
 
       <!-- Sidebar Menu -->
       <nav class="mt-2">
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
           <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
           <li class="nav-item menu-open">
             <a href="#" class="nav-link @yield('home')">
               {{-- <i class="nav-icon fas fa-tachometer-alt"></i> --}}
               <i class="nav-icon fas fa-house-user"></i>
               <p>
                 Dashboard
                 <span class="right badge badge-success">Das</span>
               </p>
             </a>
           </li>
           <li class="nav-item">
            <a href="{{url('backend/customers/index')}}" class="nav-link @yield('customer')">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Customer
                <span class="right badge badge-success">Cus</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('backend/category/index')}}" class="nav-link @yield('category')">
              <i class="nav-icon fas fa-cart-arrow-down"></i>
              <p>
                Category
                <span class="right badge badge-success">Cat</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('backend/brands/index')}}" class="nav-link @yield('brand')">
              <i class="nav-icon fab fa-bootstrap"></i>
              <p>
                Brand
                <span class="right badge badge-success">Bra</span>
              </p>
            </a>
          </li>
           <li class="nav-item">
             <a href="{{url('backend/products/index')}}" class="nav-link @yield('product')">
               <i class="nav-icon fas fa-cart-plus"></i>
               <p>
                 Product
                 <span class="right badge badge-success">Pro</span>
               </p>
             </a>
           </li>

           <li class="nav-item">
            <a href="{{url('backkend/posts/index')}}" class="nav-link @yield('post')">
              <i class="nav-icon fas fa-cloud-upload-alt"></i>
              <p>
                Post
                <span class="right badge badge-success">Pos</span>
              </p>
            </a>
          </li>
 
           <li class="nav-item ">
            <a href="{{url('register')}}" class="nav-link ">
              <i class="nav-icon fas fa-sign-in-alt"></i>
              {{-- <i class="fas fa-sign-in-alt"></i> --}}
              <p>
                Register
                <span class="right badge badge-success">Reg</span>
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{url('backend/setting/index')}}" class="nav-link @yield('setting')">
              <i class="nav-icon fas fa-user-cog"></i>
              <p>
                Setting
                <span class="right badge badge-success">Set</span>
              </p>
            </a>
          </li>

           <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-unlock"></i>
                <p> Logout 
                  <span class="right badge badge-success">Log</span>
                </p>
              </a>
          </li>
           <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                      @csrf
            </form>
           
         </ul>
       </nav>
       <!-- /.sidebar-menu -->
     </div>
     <!-- /.sidebar -->
   </aside>
 