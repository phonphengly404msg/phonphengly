<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SettingController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.master');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home',[App\Http\Controllers\LoginController::class,'index'])->name('home');
Route::get('backend/dashboard'          ,[DashboardController::class,'index']);
Route::get('backend/products/index',[ProductController::class,'index']);
Route::get('backend/customers/index',[CustomerController::class,'index']);
Route::get('backend/category/index',[CategoryController::class,'index']);
Route::get('backend/brands/index',[BrandController::class,'index']);
Route::get('backkend/posts/index',[PostController::class,'index']);
Route::get('backend/setting/index',[SettingController::class,'index']);
Route::post('backend/dashboard',[LoginController::class,'index']);


Route::get('/backend/customers/index/get/all' ,"App\Http\Controllers\CustomerController@getRow")->name('customers.get');

// Route::resource('/backend/customers/index', CustomerController::class);
Route::prefix('backend/customers/index')->group(function(){
    Route::get('/'        ,"App\Http\Controllers\CustomerController@index")->name('customers.index');
    Route::get('/get/all' ,"App\Http\Controllers\CustomerController@getRow")->name('customers.getRow');
    Route::post('/'       ,"App\Http\Controllers\CustomerController@store")->name('customers.store');
    Route::put('/{id}'    ,"App\Http\Controllers\CustomerController@update")->name('customers.update');
    Route::delete('/{id}' , "App\Http\Controllers\CustomerController@destroy")->name('customers.destroy');
});

// Route::resource('backend/category/index',CategoryController::class);
Route::prefix('backend/category/index')->group(function(){
    Route::get('/'        ,"App\Http\Controllers\CategoryController@index")->name('categories.index');
    Route::post('/'       ,"App\Http\Controllers\CategoryController@store")->name('categories.store');
    Route::get('/get/all' ,"App\Http\Controllers\CategoryController@getRow")->name('customers.getRow');
    Route::put('/{id}'    ,"App\Http\Controllers\CategoryController@update")->name('customers.update');
    Route::delete('/{id}' , "App\Http\Controllers\CategoryController@destroy")->name('customers.destroy');
});

// Route::resource('backend/products/index', ProductController::class);
Route::prefix('backend/products/index')->group(function(){
    Route::get('/' ,"App\Http\Controllers\ProductController@index")->name('products.index');
    Route::get('/get/all',"App\Http\Controllers\ProductController@getRows")->name('products.getRows');
    Route::post('/',"App\Http\Controllers\ProductController@store")->name('products.store');
});